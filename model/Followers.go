package model

/**
 * User: lizhengxiang
 * Date: 17-6-5
 * Time: 上午5:49
 */

import (
	"github.com/jinzhu/gorm"
	"g7love/database"
)

//点赞，转发，举报日志记录表
type Followers struct {
	gorm.Model
	FollowersUserid string `gorm:"size:7;not null;index:FollowersUserid"` //动态id
	Userid string `gorm:"size:10;not null;index:Userid"` //用户id
}

func Followersuser(followersUserid,userId string) int {
	var followers Followers
	followers.Userid = userId
	followers.FollowersUserid = followersUserid
	db := database.GetDB()
	var result int
	if err := db.Save(&followers).Error; err != nil {
		result = 0
	} else {
		result = 1
	}
	return result
}

func IsFollowers(followersUserid,userId string) int64 {
	var followers Followers
	var count int64
	followers.Userid = userId
	followers.FollowersUserid = followersUserid
	db := database.GetDB()
	db.Model(&followers).Where(&followers).Count(&count)
	return count
}