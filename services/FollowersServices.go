package services

/**
 * User: lizhengxiang
 * Date: 17-6-5
 * Time: 上午5:46
 */

import (
	"github.com/gin-gonic/gin"
	"g7love/model"
)

/*
 * 关注用户
 */
func Followers(c *gin.Context) Result {
	followersUserid := c.PostForm("userid")
	user :=  c.MustGet("user").(User)
	resultData := model.Followersuser(followersUserid,user.Id)
	return result(resultData,1,0)
}