package controller

/**
 * User: lizhengxiang
 * Date: 17-6-5
 * Time: 上午5:43
 */

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"g7love/services"
)
var Followers followers = followers{}
type followers struct{}

func (u *followers) Followers(c *gin.Context) {
	result := services.Followers(c)
	c.JSON(http.StatusOK, result)
}